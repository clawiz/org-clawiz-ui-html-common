/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.form.servlet.component.element.method;


import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.html.common.generator.servlet.element.AbstractJavaServletHtmlElement;
import org.clawiz.ui.html.common.generator.servlet.view.form.AbstractFormHtmlServletJavaClassComponent;
import org.clawiz.ui.html.common.generator.servlet.view.form.FormHtmlGenerator;
import org.clawiz.ui.html.common.generator.servlet.view.form.servlet.component.FormHtmlServletComponent;

public class AbstractFormHtmlServletMethodElement extends JavaMethodElement {

    private FormHtmlGenerator        formGenerator;
    private FormHtmlServletComponent formComponent;
    private Form form;


    @Override
    public void setComponent(AbstractComponent component) {
        super.setComponent(component);
        formComponent = (FormHtmlServletComponent) component;
        formGenerator = formComponent.getHtmlFormGenerator();
        form          = formComponent.getForm();
    }

    @Override
    public AbstractFormHtmlServletJavaClassComponent getComponent() {
        return formComponent;
    }

    public FormHtmlGenerator getGenerator() {
        return formGenerator;
    }

    public Form getForm() {
        return form;

    }

    public String getModelClassName() {
        return getGenerator().getModelClassName();
    }

    public String getRequestContextClassName() {
        return getGenerator().getRequestContextClassName();
    }

    public String getResponseContextClassName() {
        return getGenerator().getResponseContextClassName();
    }

    public Type getRootType() {
        return getGenerator().getRootType();
    }

    AbstractFormHtmlServletJavaClassComponent _htmlFormJavaClassComponent;

    public AbstractFormHtmlServletJavaClassComponent getHtmlFormJavaClassComponent() {
        if ( _htmlFormJavaClassComponent == null ) {
            _htmlFormJavaClassComponent = (AbstractFormHtmlServletJavaClassComponent) getComponent();
        }
        return _htmlFormJavaClassComponent;
    }

    protected <E extends AbstractJavaServletHtmlElement> AbstractJavaServletHtmlElement addLayoutComponentElement(AbstractElement parentElement, AbstractLayoutComponent layoutComponent) {
        return getHtmlFormJavaClassComponent().addLayoutComponentElement(parentElement, layoutComponent);
    }

}
