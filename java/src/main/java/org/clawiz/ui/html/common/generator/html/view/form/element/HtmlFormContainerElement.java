/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.html.view.form.element;

import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.align.HorizontalAlign;
import org.clawiz.ui.common.metadata.data.layout.component.FormLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;
import org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.html.common.generator.html.element.AbstractHtmlBodyElement;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlDivElement;

public class HtmlFormContainerElement extends HtmlDivElement {

    protected AbstractHtmlBodyElement title;
    protected AbstractHtmlBodyElement content;
    protected AbstractHtmlBodyElement formElement;
    protected AbstractHtmlBodyElement topToolbar;
    protected AbstractHtmlBodyElement bottomToolbar;

    Form form;

    @Override
    public void setLayoutComponent(AbstractLayoutComponent layoutComponent) {
        super.setLayoutComponent(layoutComponent);
        form = ((FormLayoutComponent) layoutComponent).getForm();
    }

    public Form getForm() {
        return form;
    }


    public AbstractHtmlBodyElement getTitle() {
        return title;
    }

    public AbstractHtmlBodyElement getContent() {
        return content;
    }

    public AbstractHtmlBodyElement getFormElement() {
        return formElement;
    }

    protected AbstractHtmlBodyElement createTitle() {
        return this;
    }

    protected AbstractHtmlBodyElement createContent() {
        return this;
    }

    protected AbstractHtmlBodyElement addFormElement(AbstractHtmlBodyElement content) {
        return content.addElement(org.clawiz.ui.html.common.generator.html.element.form.HtmlFormElement.class);
    }

    public AbstractHtmlBodyElement getTopToolbar() {
        return topToolbar;
    }

    public AbstractHtmlBodyElement getBottomToolbar() {
        return bottomToolbar;
    }

    protected AbstractHtmlBodyElement addToolbarPartComponent(AbstractHtmlBodyElement container, AbstractLayoutComponent component) {
        return container.addLayoutComponent(component);
    }

    protected AbstractHtmlBodyElement addToolbarPartContainer(AbstractHtmlBodyElement container, HorizontalAlign align, Toolbar toolbar) {
        return container.div();
    }

    protected void addToolbarPart(AbstractHtmlBodyElement toolbarContainer, HorizontalAlign align, Toolbar toolbar ) {
        AbstractContainer container = align == HorizontalAlign.LEFT ? toolbar.getLeft() : toolbar.getRight();
        if ( container.getComponents().size() == 0 ) {
            return;
        }

        AbstractHtmlBodyElement partContainer = addToolbarPartContainer(toolbarContainer, align, toolbar);
        for ( AbstractLayoutComponent component : container ) {
            addToolbarPartComponent(partContainer, component);
        }
    }

    protected void addToolbarParts(AbstractHtmlBodyElement container, Toolbar toolbar) {
        addToolbarPart(container, HorizontalAlign.LEFT, toolbar);
        addToolbarPart(container, HorizontalAlign.RIGHT, toolbar);
    }

    protected AbstractHtmlBodyElement addTopToolbar(AbstractHtmlBodyElement content) {
        AbstractHtmlBodyElement div = content.div();
        addToolbarParts(div, getForm().getTopToolbar());
        return div;
    }

    protected AbstractHtmlBodyElement addBottomToolbar(AbstractHtmlBodyElement content) {
        AbstractHtmlBodyElement div = content.div();
        addToolbarParts(div, getForm().getBottomToolbar());
        return div;
    }


    @Override
    public void process() {
        super.process();

        title         = createTitle();
        content       = createContent();
        formElement   = addFormElement(content);
        topToolbar    = addTopToolbar(formElement);

        for (AbstractLayoutComponent lc : getForm().getLayoutComponents() ) {
            formElement.addLayoutComponent(lc);
        }

        bottomToolbar = addBottomToolbar(formElement);

    }
}
