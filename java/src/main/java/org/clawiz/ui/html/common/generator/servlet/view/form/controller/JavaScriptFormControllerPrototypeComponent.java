/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.form.controller;


import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.LayoutComponentList;
import org.clawiz.ui.common.metadata.data.layout.component.button.Button;
import org.clawiz.ui.common.metadata.data.layout.component.field.Field;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.AbstractSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.StaticListSelectValue;
import org.clawiz.ui.html.common.generator.servlet.application.component.controller.JavaScriptAbstractFormControllerComponent;
import org.clawiz.ui.html.common.generator.servlet.view.common.component.element.javascript.JavaScriptViewControllerClassElement;
import org.clawiz.ui.html.common.generator.servlet.view.form.controller.element.JavaScriptFormControllerGetFormDataMethodElement;
import org.clawiz.ui.html.common.generator.servlet.view.form.controller.element.JavaScriptFormControllerInitMethodElement;
import org.clawiz.ui.html.common.generator.servlet.view.form.controller.element.parameters.*;

public class JavaScriptFormControllerPrototypeComponent extends AbstractJavaScriptFormControllerComponent {

    JavaScriptViewControllerClassElement      controller;

    public JavaScriptViewControllerClassElement getController() {
        return controller;
    }

    public <T extends JavaScriptFormControllerInitMethodElement> Class<T> getInitMethodClass() {
        return (Class<T>) JavaScriptFormControllerInitMethodElement.class;
    }

    protected  <T extends AbstractGetControllerParametersMethodElement> Class<T> getLayoutComponentGetControllerParametersMethodClass(AbstractLayoutComponent layoutComponent) {

        if ( layoutComponent instanceof Field) {

            Field             field     = (Field) layoutComponent;
            AbstractFormField formField = field.getFormField();

            AbstractSelectValue cvc = formField.getSelectValueContext();

            if ( cvc != null ) {
                if ( cvc instanceof StaticListSelectValue) {
                    return (Class<T>) GetStaticComboBoxControllerParametersMethodElement.class;
                } else {
                    return (Class<T>) GetDynamicComboBoxControllerParametersMethodElement.class;
                }
            }
        }

        if (layoutComponent instanceof Button ) {
            return (Class<T>) GetButtonControllerParametersMethodElement.class;
        }

        return (Class<T>) EmptyGetControllerParametersMethodElement.class;
    }

    protected void prepareGetControllerParametersMethods(LayoutComponentList components) {
        for(AbstractLayoutComponent component : components ) {

            if (getHtmlFormGenerator().getLayoutComponentControllerPath(component) != null && component.getName() != null ) {

                getController().addProperty(getLayoutComponentName(component));
                getController().addElement(
                        getLayoutComponentGetControllerParametersMethodClass(component)
                ).setLayoutComponent(component);

                getController().addGetter(getLayoutComponentName(component) + "ControllerPath")
                        .addText("return '" + getHtmlFormGenerator().getLayoutComponentControllerPath(component) + "';");

            }

            if ( component instanceof AbstractContainer) {
                prepareGetControllerParametersMethods(((AbstractContainer) component).getComponents());
            }
        }
    }

    @Override
    public void process() {
        super.process();


        throwException("Not implemented");
/*
        setName(getHtmlFormGenerator().getControllerComponent().getPrototypeName());
        setDestinationPath(getHtmlFormGenerator().getControllerComponent().getDestinationPath());

        addImport(getApplicationRequirePath() + "/controller/" + JavaScriptAbstractFormControllerComponent.ABSTRACT_FORM_CONTROLLER_CLASS_NAME);

        controller = addElement(getName(), JavaScriptViewControllerClassElement.class);
        controller
                .withExport(true)
                .withExtends(JavaScriptAbstractFormControllerComponent.ABSTRACT_FORM_CONTROLLER_CLASS_NAME);

        prepareGetControllerParametersMethods(getForm());

        controller.addElement(getInitMethodClass());

        controller.addElement(JavaScriptFormControllerGetFormDataMethodElement.class);
*/

    }
}
