/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.grid;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.server.java.view.grid.component.AbstractGridJavaClassComponent;
import org.clawiz.ui.common.generator.server.java.view.grid.ServerGridGenerator;

public class AbstractGridHtmlJavaClassComponent extends AbstractGridJavaClassComponent {

    Grid grid;

    GridHtmlGenerator _htmlGridGenerator;
    protected GridHtmlGenerator getHtmlGridGenerator() {
//        if ( _htmlGridGenerator == null ) {
//            _htmlGridGenerator = (GridHtmlGenerator) getGenerator();
//        }
        throw new CoreException("NOT IMPLEMENTED");
//        return _htmlGridGenerator;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public ServerGridGenerator getGridGenerator() {
        return (ServerGridGenerator) getGenerator();
    }

}
