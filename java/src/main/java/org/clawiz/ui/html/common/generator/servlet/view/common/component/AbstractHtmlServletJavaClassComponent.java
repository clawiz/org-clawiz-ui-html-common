/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.common.component;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;
import org.clawiz.ui.common.generator.server.java.AbstractServerJavaClassComponent;
import org.clawiz.ui.html.common.generator.html.element.HtmlTagAttribute;
import org.clawiz.ui.html.common.generator.servlet.element.AbstractJavaServletHtmlElement;

import java.util.HashMap;

public class AbstractHtmlServletJavaClassComponent extends AbstractServerJavaClassComponent {

    HashMap<Class, Class> layoutToElementMap = new HashMap<>();

    public <L extends AbstractLayoutComponent, E extends AbstractJavaServletHtmlElement> void setLayoutComponentToElementMap(Class<L> layoutComponentClass, Class<E> elementClass) {
        layoutToElementMap.put(layoutComponentClass, elementClass);
    }

    public <L extends AbstractLayoutComponent, E extends AbstractJavaServletHtmlElement> Class<E> getLayoutComponentToElementMap(Class<L> layoutComponentClass) {
        return layoutToElementMap.get(layoutComponentClass);
    }

    protected <E extends AbstractJavaServletHtmlElement> Class<E> toElementClass(AbstractLayoutComponent layoutComponent) {
        return layoutComponent != null ? layoutToElementMap.get(layoutComponent.getClass()) : null;
    }


    public <E extends AbstractJavaServletHtmlElement> E addLayoutComponentElement(AbstractElement parentElement, AbstractLayoutComponent layoutComponent) {
        Class<E> elementClass = toElementClass(layoutComponent);
        if (elementClass == null) {
            throwException("Layout component to html element map not contain value for component class '?'", new Object[]{layoutComponent.getClass().getName()});
        }
        E element = parentElement.addElement(elementClass);
        element.setLayoutComponent(layoutComponent);
        element.getAttributes().add(new HtmlTagAttribute("id", layoutComponent.getFullUid()));

        if (layoutComponent instanceof AbstractContainer) {
            for (AbstractLayoutComponent child : ((AbstractContainer) layoutComponent).getComponents()) {
                addLayoutComponentElement(element, child);
            }
        }

        return element;
    }

    protected void prepareLayoutComponentToElementMap() {
        throw new CoreException("Not implemented");

/*
        setLayoutComponentToElementMap(Form.class,          JavaServletHtmlFormElement.class);
        setLayoutComponentToElementMap(Column.class,        JavaServletHtmlColumnElement.class);
        setLayoutComponentToElementMap(TextInput.class,     JavaServletHtmTextInputlElement.class);
        setLayoutComponentToElementMap(DateInput.class,     JavaServletHtmDateInputlElement.class);
        setLayoutComponentToElementMap(DateTimeInput.class, JavaServletHtmDateTimeInputlElement.class);
        setLayoutComponentToElementMap(NumberInput.class,   JavaServletHtmlNumberInputlElement.class);
        setLayoutComponentToElementMap(DefaultButton.class, JavaServletHtmlButtonElement.class);
        setLayoutComponentToElementMap(ComboBoxInput.class, JavaServletHtmlComboBoxInputlElement.class);
*/

    }

    @Override
    public void prepare() {
        super.prepare();
        prepareLayoutComponentToElementMap();
    }
}