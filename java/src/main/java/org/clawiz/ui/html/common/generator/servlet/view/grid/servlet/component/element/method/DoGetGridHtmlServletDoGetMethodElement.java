/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.grid.servlet.component.element.method;


import org.clawiz.core.common.utils.StringUtils;

public class DoGetGridHtmlServletDoGetMethodElement extends AbstractGridHtmlServletMethodElement {

    protected String getDoMethodName() {
        return "Get";
    }

    @Override
    public void process() {
        super.process();
        setName("do" + getDoMethodName());

        addParameter("AbstractHttpRequestContext", "httpRequest");

        addText(getGenerator().getRequestContextClassName() + " request = getApi().createRequest(httpRequest);");
        addText("");
        addText("if ( request.getAction() == null ) {");
        addText("    request.setAction(\"query\");");
        addText("}");
        addText("");


        addText("if ( request.getAction().equals(\"query\")  ) {");
        addText("");
        String processMethodName = "process" + StringUtils.toUpperFirstChar(getName()) + "Query";
        addText("    " + processMethodName + "(request);");

/*
        for (String actionName : getGrid().getActionNames() ) {
            addText("");
            addText("} else if ( request.getAction().equals(\"" + actionName + "\")) {");
            addText("");
            addText("    " + getHtmlGridJavaClassComponent().actionNameToProcessMethodName(actionName) + "(request);");
        }
*/

        addText("");
        addText("} else {");
        addText("    throwException(\"Wrong " + getDoMethodName().toUpperCase() + " action  '?'\", new Object[]{request.getAction()});");
        addText("}");
    }

}
