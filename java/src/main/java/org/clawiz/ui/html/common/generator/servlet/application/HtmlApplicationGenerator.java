/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.application;


import org.clawiz.ui.common.generator.AbstractApplicationGenerator;
import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.common.generator.common.view.form.FormGenerator;
import org.clawiz.ui.common.generator.common.view.grid.GridGenerator;
import org.clawiz.ui.common.generator.common.view.screen.ScreenGenerator;
import org.clawiz.ui.common.generator.server.java.ServerJavaApplicationGeneratorContext;
import org.clawiz.ui.common.generator.server.java.application.component.ApplicationAbstractServletComponent;
import org.clawiz.ui.common.generator.server.java.application.component.ApplicationAbstractServletPrototypeComponent;
import org.clawiz.ui.common.generator.server.java.application.component.ApplicationContextPrototypeComponent;
import org.clawiz.ui.html.common.generator.servlet.application.component.*;
import org.clawiz.ui.html.common.generator.servlet.application.component.controller.*;
import org.clawiz.ui.html.common.generator.servlet.view.form.FormHtmlGenerator;
import org.clawiz.ui.html.common.generator.servlet.view.grid.GridHtmlGenerator;
import org.clawiz.ui.html.common.generator.servlet.view.screen.HtmlScreenGenerator;

public class HtmlApplicationGenerator extends AbstractApplicationGenerator {


    public <T extends AbstractApplicationGeneratorContext> Class<T> getApplicationGenerateContextClass() {
        return (Class<T>) HtmlApplicationGenerateContext.class;
    }

    public <T extends FormGenerator> Class<T> getFormGeneratorClass() {
        return (Class<T>) FormHtmlGenerator.class;
    }

    public <T extends GridGenerator> Class<T> getGridGeneratorClass() {
        return (Class<T>) GridHtmlGenerator.class;
    }

    public <T extends ScreenGenerator> Class<T> getScreenGeneratorClass() {
        return (Class<T>) HtmlScreenGenerator.class;
    }

    protected <T extends ApplicationAbstractServletComponent> Class<T> getApplicationAbstractServletComponentClass() {
        return (Class<T>) ApplicationAbstractHtmlServletComponent.class;
    }

    protected <T extends ApplicationAbstractServletPrototypeComponent> Class<T> getApplicationAbstractServletPrototypeComponentClass() {
        return (Class<T>) ApplicationAbstractHtmlServletPrototypeComponent.class;
    }

    protected <T extends JavaScriptRequireConfigComponent> Class<T> getJavaScriptRequireConfigComponentClass() {
        return (Class<T>) JavaScriptRequireConfigComponent.class;
    }

    public static final String getApplicationAssetsPath(ServerJavaApplicationGeneratorContext context) {
        return "/" + context.getPackageName().replace(".", "/") + "/assets";
    }

    @Override
    protected void process() {
        super.process();

        addComponent(ApplicationAbstractFormHtmlServletComponent.class);
        addComponent(ApplicationAbstractFormHtmlServletPrototypeComponent.class);

        addComponent(ApplicationAbstractGridHtmlServletComponent.class);
        addComponent(ApplicationAbstractGridHtmlServletPrototypeComponent.class);

        addComponent(getJavaScriptRequireConfigComponentClass());

        addComponent(JavaScriptAbstractControllerComponent.class);
        addComponent(JavaScriptAbstractControllerPrototypeComponent.class);

        addComponent(JavaScriptAbstractFormControllerComponent.class);
        addComponent(JavaScriptAbstractFormControllerPrototypeComponent.class);

        addComponent(JavaScriptAbstractGridControllerComponent.class);
        addComponent(JavaScriptAbstractGridControllerPrototypeComponent.class);
    }


    protected void addFilesToContextPrototype(String localPath, String path) {
        getComponentByClass(ApplicationContextPrototypeComponent.class).getPrepareMethod().addText("addFiles(\"" + localPath + "\", \"" + path + "\").withUseClassLoader(true);");
    }

    @Override
    protected void done() {

/*
        super.done();

        addFilesToContextPrototype(UiHtmlCommonModule.ASSETS_RESOURCE_PATH, "/assets/clawiz/common");
        addFilesToContextPrototype(getApplicationAssetsPath(getContext()), "/assets/" + getContext().getApplicationJavaClassName().toLowerCase());


        for(ServerFormGenerator generator : getFormGenerators()) {
            getComponentByClass(ApplicationContextPrototypeComponent.class).getPrepareMethod().addText("addServlet(" +
                    generator.getPackageName() + ".form." +
                    generator.getForm().getName().toLowerCase() + "." +
                    FormHtmlServletComponent.formToClassName(generator.getForm()) + ".class);");

        }

        for(ServerGridGenerator generator : getGridGenerators()) {
            getComponentByClass(ApplicationContextPrototypeComponent.class).getPrepareMethod().addText("addServlet(" +
                    getPackageName() + ".grid." + generator.getGrid().getJavaClassNameName().toLowerCase() + "." +
                    GridHtmlServletComponent.gridToClassName(generator.getGrid()) + ".class);");

        }
*/

    }
}
