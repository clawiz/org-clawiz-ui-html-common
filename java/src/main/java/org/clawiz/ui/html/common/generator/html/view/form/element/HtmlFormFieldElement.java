/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.html.view.form.element;

import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValue;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.field.Field;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.generator.common.view.form.FormGenerator;
import org.clawiz.ui.html.common.generator.html.element.AbstractHtmlBodyElement;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlDivElement;
import org.clawiz.ui.html.common.generator.html.element.form.*;

public class HtmlFormFieldElement extends HtmlDivElement {

    AbstractFormField formField;

    AbstractHtmlBodyElement fieldContainer;
    AbstractHtmlBodyElement labelContainer;
    AbstractHtmlBodyElement inputContainer;
    HtmlLabelElement        labelElement;
    HtmlInputElement        inputElement;


    @Override
    public void setLayoutComponent(AbstractLayoutComponent layoutComponent) {
        super.setLayoutComponent(layoutComponent);
        setFormField(((Field) layoutComponent).getFormField());
    }

    public AbstractFormField getFormField() {
        return formField;
    }

    public void setFormField(AbstractFormField formField) {
        this.formField = formField;
    }

    public AbstractHtmlBodyElement getFieldContainer() {
        return fieldContainer;
    }

    public AbstractHtmlBodyElement getLabelContainer() {
        return labelContainer;
    }

    public AbstractHtmlBodyElement getInputContainer() {
        return inputContainer;
    }

    public HtmlLabelElement getLabelElement() {
        return labelElement;
    }

    public HtmlInputElement getInputElement() {
        return inputElement;
    }

    protected AbstractHtmlBodyElement createFieldContainer() {
        return this;
    }

    protected AbstractHtmlBodyElement createLabelContainer(AbstractHtmlBodyElement fieldContainer) {
        return fieldContainer;
    }

    protected void addLabelRequiredSign(HtmlLabelElement label) {
        label.setText(label.getText() + " <span class='clawiz-required-label'>*</span>");
    }

    protected HtmlLabelElement createLabel(AbstractHtmlBodyElement labelContainer) {
        HtmlLabelElement label = labelContainer.addElement(HtmlLabelElement.class);
        label.setText(getFormField().getLabel());
        if ( getFormField().isRequired() ) {
            addLabelRequiredSign(label);
        }
        return label;
    }

    protected AbstractHtmlBodyElement createInputContainer(AbstractHtmlBodyElement fieldContainer) {
        return fieldContainer;
    }


    protected void addSelectOptions(HtmlSelectElement selectElement ) {
        for( ValueTypeEnumerationValue value : ((ValueTypeEnumeration) getFormField().getTypeField().getValueType()).getValues() ) {
            selectElement.addElement(HtmlSelectOptionElement.class)
                    .withValue(value.getName())
                    .withText(value.getDescription());
        }
    }

    protected HtmlInputElement createInput(AbstractHtmlBodyElement container) {

        HtmlInputElement input;
        if ( ! ( getFormField().getValueType() instanceof ValueTypeEnumeration) ) {
            input = container.addElement(HtmlInputElement.class);
        } else {
            input = container.addElement(HtmlSelectElement.class);
            addSelectOptions((HtmlSelectElement) input);
        }


        if ( getFormField().getValueType() instanceof ValueTypeBoolean ) {
            input.setType("checkbox");
        } else {
            input.setType("text");
        }

        input.addClass("clawiz-form-" + input.getType());

        input.setAttribute("name", getFormField().getJavaVariableName());
        input.setAttribute("id", ((FormGenerator) getGenerator()).getNodeId(formField));
        input.setReadOnly(formField.isReadOnly());
        return input;
    }

    public String getClickDropdownMethdoName() {
        return "click" + StringUtils.toUpperFirstChar(getFormField().getValueType().toString().toLowerCase()) + "InputDropdown";
    }

    @Override
    public void process() {
        super.process();

        fieldContainer = createFieldContainer();

        labelContainer = createLabelContainer(fieldContainer);
        labelElement   = createLabel(labelContainer);

        inputContainer = createInputContainer(fieldContainer);
        inputElement   = createInput(inputContainer);

    }
}
