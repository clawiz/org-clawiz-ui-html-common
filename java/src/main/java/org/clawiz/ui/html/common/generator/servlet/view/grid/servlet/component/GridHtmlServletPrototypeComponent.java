
/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.grid.servlet.component;


import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.server.java.view.grid.api.component.GridApiServletComponent;
import org.clawiz.ui.html.common.generator.servlet.application.component.ApplicationAbstractGridHtmlServletComponent;
import org.clawiz.ui.html.common.generator.servlet.view.grid.AbstractGridHtmlJavaClassComponent;
import org.clawiz.ui.html.common.generator.servlet.view.grid.servlet.component.element.method.*;

public class GridHtmlServletPrototypeComponent extends AbstractGridHtmlJavaClassComponent {


    public String getServletPath() {
        return "/" + StringUtils.toLowerFirstChar(getGrid().getJavaClassName());
    }


    public String actionNameToProcessMethodName(String actionName) {
        return "process" + StringUtils.toUpperFirstChar(actionName) + "Action";
    }

    protected JavaMethodElement addProcessDoQueryMethod(String name) {
        JavaMethodElement method = addMethod(name);

        method.addParameter(getGridGenerator().getRequestContextClassName(), "request");

        method.addText("writeGrid(request, executeQuery(request));");

        return method;
    }

    @Override
    public void process() {
        super.process();
        setOverwriteMode(OverwriteMode.OVERWRITE);

        setName(getHtmlGridGenerator().getHtmlServletComponent().getName() + "Prototype");

        setExtends(getGridGenerator().getApplicationGenerator().getComponentByClass(ApplicationAbstractGridHtmlServletComponent.class).getFullName());

        addImport("org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext");

        addVariable(getGridGenerator().getComponentByClass(GridApiServletComponent.class).getName(), "api").setAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);
        JavaMethodElement getApiMethod = addMethod("getApi");
        getApiMethod.setAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        getApiMethod.setType(getGridGenerator().getComponentByClass(GridApiServletComponent.class).getName());
        getApiMethod.addText("return api;");

        JavaMethodElement pathMethod = addMethod("String", "getPath");
        pathMethod.addText("return \"" + getServletPath() + "\";");

        addElement(WriteBeforeGridHtmlServletMethodElement.class);
        addElement(WriteGridHeaderGridHtmlServletMethodElement.class);
        addElement(WriteGridRowGridHtmlServletMethodElement.class);
        addElement(WriteGridBodyGridHtmlServletMethodElement.class);
        addElement(WriteGridFooterGridHtmlServletMethodElement.class);
        addElement(WriteAfterGridGridHtmlServletMethodElement.class);

        addElement(WriteGridGridHtmlServletMethodElement.class);

        addElement(ExecuteQueryGridHtmlServletMethodElement.class);
        addProcessDoQueryMethod("processDoGetQuery");
        addProcessDoQueryMethod("processDoPostQuery");

/*
        for (String actionName : getGrid().getActionNames() ) {
            ProcessActionGridHtmlServletMethodElement processActionMethodElement = addElement(ProcessActionGridHtmlServletMethodElement.class);
            processActionMethodElement.setActionName(actionName);
        }
*/

        addElement(DoGetGridHtmlServletDoGetMethodElement.class);
        addElement(DoPostGridHtmlServletDoPostMethodElement.class);

    }

}
