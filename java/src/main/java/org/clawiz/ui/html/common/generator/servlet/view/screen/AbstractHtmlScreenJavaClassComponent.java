/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.screen;


import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.ui.common.metadata.data.view.screen.Screen;
import org.clawiz.ui.html.common.generator.servlet.view.common.component.AbstractHtmlServletJavaClassComponent;

import static org.clawiz.ui.common.generator.server.java.view.screen.ServerScreenGenerator.SCREENS_PACKAGE_NAME;

public class AbstractHtmlScreenJavaClassComponent extends AbstractHtmlServletJavaClassComponent {

    Screen screen;

    HtmlScreenGenerator _htmlScreenGenerator;
    protected HtmlScreenGenerator getHtmlScreenGenerator() {
        if ( _htmlScreenGenerator == null ) {
            _htmlScreenGenerator = (HtmlScreenGenerator) getGenerator();
        }
        return _htmlScreenGenerator;
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    public HtmlScreenGenerator getScreenGenerator() {
        return (HtmlScreenGenerator) getGenerator();
    }

    @Override
    public void process() {
        super.process();

        setScreen(getScreenGenerator().getScreen());

        setPackageName(getScreenGenerator().getPackageName() + "." + SCREENS_PACKAGE_NAME + "." + GeneratorUtils.toJavaClassName(getScreen().getName()).toLowerCase());
    }

}
