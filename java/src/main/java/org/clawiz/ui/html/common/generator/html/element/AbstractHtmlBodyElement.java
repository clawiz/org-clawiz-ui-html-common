/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.html.element;

import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlAnchorElement;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlButtonElement;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlDivElement;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlSpanElement;
import org.clawiz.ui.html.common.generator.html.element.table.HtmlTableElement;

public class AbstractHtmlBodyElement extends AbstractHtmlElement {

    String          onClickExpression;
    String          visibleExpression;
    String          disabledExpression;


    AbstractLayoutComponent layoutComponent;

    public AbstractLayoutComponent getLayoutComponent() {
        return layoutComponent;
    }

    public void setLayoutComponent(AbstractLayoutComponent layoutComponent) {
        this.layoutComponent = layoutComponent;
    }

    public AbstractHtmlBodyElement addLayoutComponent(AbstractLayoutComponent layoutComponent) {
        return getComponent().addLayoutComponent(this, layoutComponent, true);
    }
    
    public void setClass(String className) {
        setAttribute("class", className);
    }

    public void addClass(String className) {
        if ( className == null ) {
            return;
        }
        String value = getAttributes().get("class");
        setClass((value != null ? value + " " : "") + className );
    }

    @Override
    public AbstractHtmlBodyElement withAttribute(String name, String value) {
        super.withAttribute(name, value);
        return this;
    }

    public void addAttribute(String name) {
        super.addAttribute(name, null);
    }

    public void addAttribute(String name, String value) {
        super.addAttribute(name, value);
    }

    public void setRole(String role) {
        setAttribute("role", role);
    }

    public void setType(String type) {
        setAttribute("type", type);
    }

    public AbstractHtmlBodyElement withText(String text) {
        setText(text);
        return this;
    }

    public AbstractHtmlBodyElement withClass(String _class) {
        setClass(_class);
        return this;
    }

    public AbstractHtmlBodyElement withSelfCloseEnabled(boolean value) {
        setSelfCloseEnabled(value);
        return this;
    }

    public String getOnClickExpression() {
        return onClickExpression != null ? onClickExpression : ( getLayoutComponent() != null ? getLayoutComponent().getOnClickExpression() : null);
    }

    public void setOnClickExpression(String onClickExpression) {
        this.onClickExpression = onClickExpression;
    }

    public String getVisibleExpression() {
        return visibleExpression != null ? visibleExpression : ( getLayoutComponent() != null ? getLayoutComponent().getVisibleExpression() : null);
    }

    public void setVisibleExpression(String visibleExpression) {
        this.visibleExpression = visibleExpression;
    }

    public String getDisabledExpression() {
        return disabledExpression != null ? disabledExpression : ( getLayoutComponent() != null ? getLayoutComponent().getDisabledExpression() : null);
    }

    public void setDisabledExpression(String disabledExpression) {
        this.disabledExpression = disabledExpression;
    }

    public HtmlDivElement div() {
        return addElement(HtmlDivElement.class);
    }

    public HtmlTableElement table() {
        return addElement(HtmlTableElement.class);
    }

    public HtmlAnchorElement a() { return addElement(HtmlAnchorElement.class); }

    public HtmlSpanElement span() {
        return addElement(HtmlSpanElement.class);
    }

    public HtmlButtonElement button() {
        return addElement(HtmlButtonElement.class);
    }


    protected String getOnClickAttributeName() {
        return "click";
    }

    public void addOnclick() {
        if ( getOnClickExpression() != null ) {
            addAttribute(getOnClickAttributeName(), getOnClickExpression());
        }
    }

    protected String getVisibleAttributeName() {
        return "click";
    }

    public void addVisible() {
        if ( getVisibleExpression() != null ) {
            addAttribute(getVisibleAttributeName(), getVisibleExpression());
        }
    }

    protected String getDisabledAttributeName() {
        return "click";
    }

    public void addDisabled() {
        if ( getDisabledExpression() != null ) {
            addAttribute(getDisabledAttributeName(), getDisabledExpression());
        }
    }

    public void prepareElementSizes() {

    }

    @Override
    public void process() {
        super.process();

        prepareElementSizes();

        addOnclick();
        addVisible();
        addDisabled();

    }
    
}
