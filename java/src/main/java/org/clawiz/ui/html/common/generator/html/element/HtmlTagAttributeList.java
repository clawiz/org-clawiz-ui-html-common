/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.html.element;

import org.clawiz.core.common.CoreException;

import java.util.ArrayList;
import java.util.HashMap;

public class HtmlTagAttributeList extends ArrayList<HtmlTagAttribute> {

    private HashMap<String, HtmlTagAttribute> namesCache = new HashMap<>();

    public void set(String name) {
        set(name, null);
    }

    public void set(String name, String value) {
        if ( name == null ) {
            throw new CoreException("Cannot get html tag attribute with null name");
        }
        String key = name.toUpperCase();
        HtmlTagAttribute attribute = namesCache.get(key);
        if ( attribute == null ) {
            attribute = new HtmlTagAttribute(name, value);
            add(attribute);
            namesCache.put(key, attribute);
        } else {
            attribute.setValue(value);
        }
    }

    public void unset(String name) {
        String key = name.toUpperCase();
        if ( ! namesCache.containsKey(key)) {
            return;
        }
        int index = -1;
        for (int i=0; i < size(); i++) {
            if ( get(i).getName().toUpperCase().equals(key)) {
                index = i;
                break;
            }
        }
        namesCache.remove(key);
        remove(index);

    }

    public String get(String name) {
        if ( name == null) {
            return null;
        }
        HtmlTagAttribute attribute = namesCache.get(name.toUpperCase());
        return attribute != null ? attribute.getValue() : null;
    }

}
