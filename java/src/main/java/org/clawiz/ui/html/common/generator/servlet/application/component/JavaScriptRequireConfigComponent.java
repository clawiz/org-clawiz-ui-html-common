/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.application.component;


public class JavaScriptRequireConfigComponent extends AbstractJavaScriptApplicationComponent {


    protected void addCommonRequirePaths() {
       addText("        \"clawiz\": \"/assets/clawiz/common/js\",");
    }

    protected void addApplicationRequirePath() {
        addText("        \"" + getApplicationContext().getApplicationJavaClassName().toLowerCase() + "\": \""
                + "/assets/" + getApplicationContext().getApplicationJavaClassName().toLowerCase()
                +"/js\"");
    }

    @Override
    public void process() {
        super.process();

        setName("require-config");
        setFileName(getName() + ".js");

        addText("require.config({\n" +
                "    paths: {\n");

        addCommonRequirePaths();
        addApplicationRequirePath();

        addText("    }\n" +
                "});\n");
    }
}
