/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.html.view.grid.element;

import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.align.HorizontalAlign;
import org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign;
import org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;
import org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn;
import org.clawiz.ui.html.common.generator.html.element.AbstractHtmlBodyElement;
import org.clawiz.ui.html.common.generator.html.element.table.*;

public class HtmlGridContainerElement extends AbstractHtmlBodyElement {


    Grid grid;

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    protected AbstractHtmlBodyElement       title;
    protected AbstractHtmlBodyElement       content;
    protected AbstractHtmlBodyElement       topToolbar;
    protected AbstractHtmlBodyElement       bottomToolbar;
    protected HtmlTableElement              table;
    protected HtmlTableHeaderElement        tableHeader;
    protected HtmlTableBodyElement          tableBody;

    @Override
    public void setLayoutComponent(AbstractLayoutComponent layoutComponent) {
        super.setLayoutComponent(layoutComponent);
        setGrid(((GridLayoutComponent) layoutComponent).getGrid());
    }

    public AbstractHtmlBodyElement getTitle() {
        return title;
    }

    public AbstractHtmlBodyElement getContent() {
        return content;
    }

    public HtmlTableElement getTable() {
        return table;
    }

    public HtmlTableHeaderElement getTableHeader() {
        return tableHeader;
    }

    public HtmlTableBodyElement getTableBody() {
        return tableBody;
    }

    protected AbstractHtmlBodyElement addTitle() {
        return this;
    }

    protected AbstractHtmlBodyElement addContent() {
        return this;
    }

    protected HtmlTableElement addTable(AbstractHtmlBodyElement content) {
        return content.table();
    }

    protected String getHorizontalAlignToClass(HorizontalAlign align) {
        return null;
    }

    protected String getVerticalAlignToClass(VerticalAlign align) {
        return null;
    }

    protected HtmlTableHeaderColumnElement addHeaderColumn(HtmlTableHeaderElement thead, AbstractGridColumn column) {

        HtmlTableHeaderRowElement tr = thead.getElementsByClass(HtmlTableHeaderRowElement.class).get(0);
        HtmlTableHeaderColumnElement th = tr.th();

        th.setText(column.getCaption());
        th.addClass(getHorizontalAlignToClass(HorizontalAlign.CENTER));

        return th;
    }

    protected HtmlTableHeaderColumnElement addHeaderActionsColumn(HtmlTableHeaderElement thead) {

        HtmlTableHeaderRowElement tr = thead.getElementsByClass(HtmlTableHeaderRowElement.class).get(0);
        HtmlTableHeaderColumnElement th = tr.th();

        th.addClass(getHorizontalAlignToClass(HorizontalAlign.CENTER));

        return th;
    }

    protected HtmlTableHeaderRowElement addHeaderRow(HtmlTableHeaderElement thead) {

        HtmlTableHeaderRowElement tr = thead.tr();

        for (AbstractGridColumn column : grid.getColumns()) {
            addHeaderColumn(thead, column);
        }

        return tr;
    }



    protected HtmlTableHeaderElement addTableHeader(HtmlTableElement table) {
        HtmlTableHeaderElement thead = table.thead();

        addHeaderRow(thead);

        return thead;
    }

    protected HtmlTableDataElement addDataColumn(HtmlTableRowElement tr, AbstractGridColumn column) {
        HtmlTableDataElement td = tr.td();

        if ( column.getHorizontalAlign() != null ) {
            td.addClass(getHorizontalAlignToClass(column.getHorizontalAlign()));
        }

        if ( column.getVerticalAlign() != null ) {
            td.addClass(getVerticalAlignToClass(column.getVerticalAlign()));
        }

        return td;
    }

    protected void setTableBodyRowOnclick(HtmlTableRowElement rowElement) {

    }

    protected HtmlTableRowElement addTableBodyRow(HtmlTableBodyElement body) {
        HtmlTableRowElement tr = body.tr();

        for(AbstractGridColumn column : getGrid().getColumns() ) {
            addDataColumn(tr, column);
        }

        setTableBodyRowOnclick(tr);

        return tr;
    }

    protected void addTableBodyRows(HtmlTableBodyElement body) {
        addTableBodyRow(body);
    }

    protected HtmlTableBodyElement addTableBody(HtmlTableElement table) {
        HtmlTableBodyElement body = table.tbody();

        addTableBodyRows(body);

        return body;
    }

    protected AbstractHtmlBodyElement addToolbarPartComponent(AbstractHtmlBodyElement container, AbstractLayoutComponent component) {
        return container.addLayoutComponent(component);
    }

    protected AbstractHtmlBodyElement addToolbarPartContainer(AbstractHtmlBodyElement container, HorizontalAlign align, Toolbar toolbar) {
        return container.div();
    }

    protected void addToolbarPart(AbstractHtmlBodyElement toolbarContainer, HorizontalAlign align, Toolbar toolbar ) {
        AbstractContainer container = align == HorizontalAlign.LEFT ? toolbar.getLeft() : toolbar.getRight();
        if ( container.getComponents().size() == 0 ) {
            return;
        }

        AbstractHtmlBodyElement partContainer = addToolbarPartContainer(toolbarContainer, align, toolbar);
        for ( AbstractLayoutComponent component : container ) {
            addToolbarPartComponent(partContainer, component);
        }
    }

    protected void addToolbarParts(AbstractHtmlBodyElement container, Toolbar toolbar) {
        addToolbarPart(container, HorizontalAlign.LEFT, toolbar);
        addToolbarPart(container, HorizontalAlign.RIGHT, toolbar);
    }

    protected AbstractHtmlBodyElement addTopToolbar(AbstractHtmlBodyElement content) {
        AbstractHtmlBodyElement div = content.div();
        addToolbarParts(div, getGrid().getTopToolbar());
        return div;
    }

    protected AbstractHtmlBodyElement addBottomToolbar(AbstractHtmlBodyElement content) {
        AbstractHtmlBodyElement div = content.div();
        addToolbarParts(div, getGrid().getBottomToolbar());
        return div;
    }

    @Override
    public void process() {
        super.process();

        setTagName("div");

        title         = addTitle();
        content       = addContent();
        topToolbar    = addTopToolbar(content);
        table         = addTable(content);
        tableHeader   = addTableHeader(table);
        tableBody     = addTableBody(table);
        bottomToolbar = addBottomToolbar(content);

    }

}
