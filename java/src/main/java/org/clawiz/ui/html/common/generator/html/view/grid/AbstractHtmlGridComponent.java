/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.html.view.grid;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.utils.file.FileUtils;
import org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGenerator;
import org.clawiz.ui.common.generator.client.ts.ui.grid.TypeScriptUiGridGenerator;
import org.clawiz.ui.html.common.generator.html.AbstractHtmlComponent;
import org.clawiz.ui.html.common.generator.html.view.grid.element.HtmlGridContainerElement;

public class AbstractHtmlGridComponent extends AbstractHtmlComponent {


    TypeScriptUiGridGenerator gridGenerator;
    HtmlGridContainerElement gridElement;

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        gridGenerator = (TypeScriptUiGridGenerator) generator;
    }

    @Override
    public TypeScriptUiGridGenerator getGenerator() {
        return gridGenerator;
    }

    protected TypeScriptClientCommonGenerator getApplicationGenerator() {
        return getGenerator().getContext().getApplicationGenerator();
    }

    protected Grid getGrid() {
        return getGenerator().getGrid();
    }

    @Override
    public String getDestinationPath() {
        return getApplicationGenerator().getApplicationDestinationPath()
                + GeneratorUtils.getPackageRelativePath(getApplicationGenerator().getPackageName(), getGrid().getPackageName())
                + "/" + getGrid().getJavaClassName().toLowerCase();
    }

    public HtmlGridContainerElement getGridElement() {
        return gridElement;
    }

    @Override
    protected void prepareLayoutComponentsMap() {
        super.prepareLayoutComponentsMap();
        addLayoutComponentMap(GridLayoutComponent.class, HtmlGridContainerElement.class);
    }

    protected void addLayoutElements() {
        GridLayoutComponent gridLayoutComponent = new GridLayoutComponent();
        gridLayoutComponent.setGrid(getGrid());
        gridElement = (HtmlGridContainerElement) addLayoutComponent(gridLayoutComponent);
    }

    @Override
    public void process() {
        super.process();

        setName(FileUtils.nameToDottedFileName(getGrid().getJavaClassName()));

        addLayoutElements();

    }

}
