/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.js.common.component;


import org.clawiz.core.common.system.generator.es6.component.AbstractES6Component;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.AbstractUiGenerator;
import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;

import java.io.File;

public abstract class AbstractJavaScriptApplicationAssetComponent extends AbstractES6Component {

    public static final String CLAWIZ_UI_CONTROLLER_ASSET_PATH = "clawiz/ui/controller";

    public abstract AbstractApplicationGeneratorContext getApplicationContext();

    protected String getJavaScriptAssetsTargetPath(String packageName) {

        String applicationPackageName = getApplicationContext().getPackageName();

        String path = getDestinationPath() + File.separator + "main" + File.separator + "resources"
                + File.separator
                + applicationPackageName.replace(".", File.separator)
                + File.separator + "assets" + File.separator + "js";

        if ( ! StringUtils.substring(packageName, 0, applicationPackageName.length()).equals(applicationPackageName) ) {
            throwException("Application asset javascript component package name '?' not start from application package name '?'", new Object[]{packageName, applicationPackageName});
        }

        if ( applicationPackageName.length() == packageName.length() ) {
            return path;
        }

        if ( ! StringUtils.substring(packageName, 0, applicationPackageName.length()+1).equals(applicationPackageName + ".") ) {
            throwException("Application asset javascript component package name '?' not start from application package name '?'", new Object[]{packageName, applicationPackageName});
        }

        path = path + File.separator + packageName.substring(applicationPackageName.length()+1).replace(".", File.separator);


        return path;
    }

    protected String getJavaScriptAssetsTargetPath() {
        return  getJavaScriptAssetsTargetPath(((AbstractUiGenerator) getGenerator()).getPackageName());
    }

    @Override
    public void process() {
        super.process();

        setDestinationPath(getJavaScriptAssetsTargetPath());
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        setFileName(name + ".es6");
    }



}
