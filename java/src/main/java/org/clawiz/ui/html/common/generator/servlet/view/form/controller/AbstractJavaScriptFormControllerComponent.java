/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.form.controller;


import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.html.common.generator.servlet.view.common.component.AbstractJavaScriptViewControllerComponent;
import org.clawiz.ui.html.common.generator.servlet.view.form.FormHtmlGenerator;

import java.io.File;

public class AbstractJavaScriptFormControllerComponent extends AbstractJavaScriptViewControllerComponent {

    @Override
    protected String getJavaScriptAssetsTargetPath() {
        return super.getJavaScriptAssetsTargetPath() + File.separator + "form";
    }

    Form form;

    public Form getForm() {
        return form;
    }

    public String getControllerClassName() {
        Form form = getHtmlFormGenerator().getContext().getForm();
        return form.getJavaClassName() + "Controller";
    }

    public String getRequirePath() {
        String path = getApplicationContext().getApplicationJavaClassName().toLowerCase() + "/";

        String applicationPackageName =  getApplicationContext().getPackageName();
        String formPackageName         = getHtmlFormGenerator().getContext().getPackageName();

        if ( formPackageName.length() > applicationPackageName.length() ) {
            path += formPackageName.substring(applicationPackageName.length()+1).replace(".", "/") + "/";
        }

        Form form = getHtmlFormGenerator().getContext().getForm();

        path += "controller/form/" + form.getJavaClassName().toLowerCase() + "/" + getControllerClassName();

        return path;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    FormHtmlGenerator _htmlFormGenerator;
    protected FormHtmlGenerator getHtmlFormGenerator() {
        if ( _htmlFormGenerator == null ) {
            _htmlFormGenerator = (FormHtmlGenerator) getGenerator();
        }
        return _htmlFormGenerator;
    }

    @Override
    public AbstractApplicationGeneratorContext getApplicationContext() {
        return getHtmlFormGenerator().getContext().getApplicationContext();
    }

    protected String getApplicationRequirePath() {
        return getApplicationContext().getApplicationJavaClassName().toLowerCase();
    }

    @Override
    public void process() {
        super.process();

        setForm(getHtmlFormGenerator().getForm());

    }

    public String getLayoutComponentName(AbstractLayoutComponent component) {
        return StringUtils.toLowerFirstChar(component.getName());
    }

    public String getLayoutComponentControllerName(AbstractLayoutComponent component) {
        return getLayoutComponentName(component) + "Controller";
    }

    public String getLayoutComponentId(AbstractLayoutComponent component) {
        return component.getFullUid();
    }

}
