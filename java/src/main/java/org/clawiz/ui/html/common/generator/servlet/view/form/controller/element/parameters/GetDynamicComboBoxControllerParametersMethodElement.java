/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.form.controller.element.parameters;


import org.clawiz.ui.common.metadata.data.layout.component.field.Field;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.AbstractSelectValue;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.FormApiServletPrototypeComponent;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.element.choosevaluerequest.AbstractFormApiServletQuerySelectValuesMethodElement;

public class GetDynamicComboBoxControllerParametersMethodElement extends AbstractGetControllerParametersMethodElement {

    @Override
    public void process() {
        super.process();
        Field             field     = (Field) getLayoutComponent();
        AbstractFormField formField = field.getFormField();
        AbstractSelectValue cvc = formField.getSelectValueContext();
        addText("return {");
        addText("    api : {");
        addText("              path   : '" + FormApiServletPrototypeComponent.getServletPath(getHtmlFormGenerator().getForm()) + "',");
        addText("              action : '" + AbstractFormApiServletQuerySelectValuesMethodElement.getQuerySelectValuesMethodName(formField) + "'");
        addText("          }");
        addText("};");

    }
}
