/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.form.controller.element;

import org.clawiz.core.common.system.generator.es6.component.element.ES6ClassMethodElement;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.html.common.generator.servlet.view.form.FormHtmlGenerator;
import org.clawiz.ui.html.common.generator.servlet.view.form.controller.AbstractJavaScriptFormControllerComponent;

public class AbstractJavaScriptFormControllerClassMethodElement extends ES6ClassMethodElement {

    AbstractJavaScriptFormControllerComponent _formControllerComponent;
    public AbstractJavaScriptFormControllerComponent getFormControllerComponent() {
        if ( _formControllerComponent == null) {
            _formControllerComponent = (AbstractJavaScriptFormControllerComponent) getComponent();
        }
        return _formControllerComponent;
    }

    Form _form;
    public Form getForm() {
        if ( _form == null ) {
            _form = getFormControllerComponent().getForm();
        }
        return _form;
    }

    FormHtmlGenerator _htmlFormGenerator;
    public FormHtmlGenerator getHtmlFormGenerator() {
        if ( _htmlFormGenerator == null ) {
            _htmlFormGenerator = (FormHtmlGenerator) getGenerator();
        }
        return _htmlFormGenerator;
    }

    public String getLayoutComponentName(AbstractLayoutComponent component) {
        return getFormControllerComponent().getLayoutComponentName(component);
    }

    public String getLayoutComponentId(AbstractLayoutComponent component) {
        return getFormControllerComponent().getLayoutComponentId(component);
    }


}
