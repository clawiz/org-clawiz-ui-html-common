/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.form;


import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.generator.server.java.view.form.ServerFormGenerator;
import org.clawiz.ui.html.common.generator.servlet.view.common.component.AbstractHtmlServletJavaClassComponent;

public class AbstractFormHtmlServletJavaClassComponent extends AbstractHtmlServletJavaClassComponent {

    Form form;

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public ServerFormGenerator getFormGenerator() {
        return (ServerFormGenerator) getGenerator();
    }

    FormHtmlGenerator _htmlFormGenerator;
    public FormHtmlGenerator getHtmlFormGenerator() {
        if ( _htmlFormGenerator == null ) {
            _htmlFormGenerator = (FormHtmlGenerator) getGenerator();
        }
        return _htmlFormGenerator;
    }

    @Override
    public void process() {
        super.process();

        setForm(getFormGenerator().getForm());

        setPackageName(getForm().getPackageName() + "." + getForm().getJavaClassName().toLowerCase());
    }


}
