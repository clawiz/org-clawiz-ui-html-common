/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.html.view.form;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.utils.file.FileUtils;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.FormLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.field.Field;
import org.clawiz.ui.common.metadata.data.layout.container.column.Column;
import org.clawiz.ui.common.metadata.data.layout.container.row.Row;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGenerator;
import org.clawiz.ui.common.generator.client.ts.ui.form.TypeScriptUiFormGenerator;
import org.clawiz.ui.html.common.generator.html.AbstractHtmlComponent;
import org.clawiz.ui.html.common.generator.html.view.form.element.HtmlFormColumnElement;
import org.clawiz.ui.html.common.generator.html.view.form.element.HtmlFormFieldElement;
import org.clawiz.ui.html.common.generator.html.view.form.element.HtmlFormContainerElement;
import org.clawiz.ui.html.common.generator.html.view.form.element.HtmlFormRowElement;

public class AbstractHtmlFormComponent extends AbstractHtmlComponent {

    TypeScriptUiFormGenerator formGenerator;
    HtmlFormContainerElement formElement;

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        formGenerator = (TypeScriptUiFormGenerator) generator;
    }

    @Override
    public TypeScriptUiFormGenerator getGenerator() {
        return formGenerator;
    }

    protected TypeScriptClientCommonGenerator getApplicationGenerator() {
        return getGenerator().getContext().getApplicationGenerator();
    }

    protected Form getForm() {
        return getGenerator().getForm();
    }

    @Override
    public String getDestinationPath() {
        return getApplicationGenerator().getApplicationDestinationPath()
                + GeneratorUtils.getPackageRelativePath(getApplicationGenerator().getPackageName(), getForm().getPackageName())
                + "/" + getForm().getJavaClassName().toLowerCase();
    }

    public HtmlFormContainerElement getFormElement() {
        return formElement;
    }

    @Override
    protected void prepareLayoutComponentsMap() {
        super.prepareLayoutComponentsMap();

        addLayoutComponentMap(FormLayoutComponent.class, HtmlFormContainerElement.class);
        addLayoutComponentMap(Field.class, HtmlFormFieldElement.class);
        addLayoutComponentMap(Row.class, HtmlFormRowElement.class);
        addLayoutComponentMap(Column.class, HtmlFormColumnElement.class);
    }

    protected void addLayoutElements() {
        FormLayoutComponent formLayoutComponent = new FormLayoutComponent();
        formLayoutComponent.setForm(getForm());
        formElement = (HtmlFormContainerElement) addLayoutComponent(null, formLayoutComponent, false);
    }

    @Override
    public void process() {
        super.process();

        setName(FileUtils.nameToDottedFileName(getForm().getJavaClassName()));

        addLayoutElements();

    }

}
