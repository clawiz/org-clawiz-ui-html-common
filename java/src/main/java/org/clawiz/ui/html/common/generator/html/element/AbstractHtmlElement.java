/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.html.element;


import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.file.component.element.StringTextElement;
import org.clawiz.ui.html.common.generator.html.AbstractHtmlComponent;

public abstract class AbstractHtmlElement extends StringTextElement {


    String          tagName;

    String          text;
    boolean         printTextOnNewLine = false;
    boolean         selfCloseEnabled   = true;
    boolean         closeable          = true;

    HtmlTagAttributeList  attributes = new HtmlTagAttributeList();

    AbstractHtmlComponent htmlComponent;
    @Override
    public AbstractHtmlComponent getComponent() {
        return htmlComponent;
    }

    @Override
    public void setComponent(AbstractComponent component) {
        super.setComponent(component);
        htmlComponent = (AbstractHtmlComponent) component;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public HtmlTagAttributeList getAttributes() {
        return attributes;
    }

    public void setAttribute(String name) {
        setAttribute(name, null);
    }

    public void setAttribute(String name, String value) {
        attributes.set(name, value);
    }

    public AbstractHtmlElement withAttribute(String name, String value) {
        setAttribute(name, value);
        return this;
    }

    public void removeAttribute(String name) {
        attributes.remove(name);
    }


    public void addAttribute(String name, String value) {
        setAttribute(name, value);
    }

    public String getAttribute(String name) {
        return attributes.get(name);
    }

    public void clearAttribute(String name) {
        attributes.unset(name);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public AbstractHtmlElement withText(String text) {
        setText(text);
        return this;
    }

    public boolean isPrintTextOnNewLine() {
        return printTextOnNewLine;
    }

    public void setPrintTextOnNewLine(boolean printTextOnNewLine) {
        this.printTextOnNewLine = printTextOnNewLine;
    }

    public boolean isSelfCloseEnabled() {
        return selfCloseEnabled;
    }

    public void setSelfCloseEnabled(boolean selfCloseEnabled) {
        this.selfCloseEnabled = selfCloseEnabled;
    }

    public boolean isCloseable() {
        return closeable;
    }

    public void setCloseable(boolean closeable) {
        this.closeable = closeable;
    }

    @Override
    public <T extends AbstractElement> T addElement(Class<T> clazz, String childName) {
        T element = super.addElement(clazz, childName);
        ((StringTextElement) element).setLpad(getLpad() + "  ");
        return element;
    }

    protected String encodeAttributeValueQuotes(String str) {
        return str.replace("\"","&quot;");
    }

    @Override
    public void write() {


        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getTagName());
        for ( HtmlTagAttribute a : getAttributes() ) {
            sb.append(" ")
                    .append(a.getName());
            if ( a.getValue() != null ) {
                sb.append("=\"")
                        .append(encodeAttributeValueQuotes(a.getValue()))
                        .append("\"");
            }
        }

        if ( ! closeable ) {
            if ( getElements().size() > 0 ) {
                throwException("Not closeable tag cannot have child elements");
            }
            if ( getText() != null ) {
                throwException("Not closeable tag cannot have text");
            }
            pln(sb.append(">").toString());
            return;
        }

        if ( getElements().size() > 0 || ! selfCloseEnabled ) {
            pln(sb.append(">").toString()
                    + ( getText() != null && printTextOnNewLine ? "\n" + getLpad() : "")
                    + ( getText() != null ? getText() : "")
                    + ( getElements().size() > 0 ? ""
                        : "</" + getTagName() + ">" )
            );
            if ( getElements().size() > 0 ) {
                writeElements();
                pln("</" + getTagName() + ">");
            }
        } else if ( getText() != null ) {
            pln(sb.append(">").toString()
                    + (printTextOnNewLine ? "\n" + getLpad() + "  " : "")
                    + getText()
                    + (printTextOnNewLine ? "\n" + getLpad() : "")
                    + "</" + getTagName() + ">");
        } else {
            pln(sb.append("/>").toString());
        }
    }
}
