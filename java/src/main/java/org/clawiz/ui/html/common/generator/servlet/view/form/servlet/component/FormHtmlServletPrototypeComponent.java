
/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.form.servlet.component;


import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.FormApiServletComponent;
import org.clawiz.ui.html.common.generator.servlet.application.component.ApplicationAbstractFormHtmlServletComponent;
import org.clawiz.ui.html.common.generator.servlet.view.form.servlet.component.element.method.*;
import org.clawiz.ui.html.common.generator.servlet.view.form.AbstractFormHtmlServletJavaClassComponent;

public class FormHtmlServletPrototypeComponent extends AbstractFormHtmlServletJavaClassComponent {

    WriteBeforeFormFormHtmlServletMethodElement writeBeforeForm;
    WriteFormBodyFormHtmlServletMethodElement writeFormBody;
    WriteAfterFormFormHtmlServletMethodElement writeAfterForm;

    WriteFormFormHtmlServletMethodElement writeForm;

    DoGetFormHtmlServletMethodElement doGet;
    DoPostFormHtmlServletDoPostMethodElement doPost;

    ProcessDoGetFormHtmlServletMethodElement processDoGet;
    ProcessDoPostFormHtmlServletMethodElement processDoPost;

    public String getServletPath() {
        return "/" + StringUtils.toLowerFirstChar(getForm().getJavaClassName());
    }

    public WriteBeforeFormFormHtmlServletMethodElement getWriteBeforeForm() {
        return writeBeforeForm;
    }

    public WriteFormBodyFormHtmlServletMethodElement getWriteFormBody() {
        return writeFormBody;
    }

    public WriteAfterFormFormHtmlServletMethodElement getWriteAfterForm() {
        return writeAfterForm;
    }

    public WriteFormFormHtmlServletMethodElement getWriteForm() {
        return writeForm;
    }

    public DoGetFormHtmlServletMethodElement getDoGet() {
        return doGet;
    }

    public DoGetFormHtmlServletMethodElement getDoPost() {
        return doPost;
    }

    @Override
    public void process() {
        super.process();
        setOverwriteMode(OverwriteMode.OVERWRITE);

        setName(getHtmlFormGenerator().getHtmlServletComponent().getName() + "Prototype");

        setExtends(getFormGenerator().getApplicationGenerator().getComponentByClass(ApplicationAbstractFormHtmlServletComponent.class).getFullName());

        addImport("org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext");

        addVariable(getFormGenerator().getComponentByClass(FormApiServletComponent.class).getName(), "api").setAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);
        JavaMethodElement getApiMethod = addMethod("getApi");
        getApiMethod.setAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        getApiMethod.setType(getFormGenerator().getComponentByClass(FormApiServletComponent.class).getName());
        getApiMethod.addText("return this.api;");

        JavaMethodElement pathMethod = addMethod("String", "getPath");
        pathMethod.addText("return \"" + getServletPath() + "\";");

        writeBeforeForm = addElement(WriteBeforeFormFormHtmlServletMethodElement.class);
        writeFormBody   = addElement(WriteFormBodyFormHtmlServletMethodElement.class);
        writeAfterForm  = addElement(WriteAfterFormFormHtmlServletMethodElement.class);

        writeForm       = addElement(WriteFormFormHtmlServletMethodElement.class);

        addElement(GetLoadControllerStatementFormHtmlServletMethodElement.class);

        processDoGet    = addElement(ProcessDoGetFormHtmlServletMethodElement.class);
        processDoPost   = addElement(ProcessDoPostFormHtmlServletMethodElement.class);

        doGet  = addElement(DoGetFormHtmlServletMethodElement.class);
        doPost = addElement(DoPostFormHtmlServletDoPostMethodElement.class);
    }

}
