/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.element;

import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.file.component.element.StringTextElement;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.html.common.generator.html.element.HtmlTagAttribute;
import org.clawiz.ui.html.common.generator.html.element.HtmlTagAttributeList;

public class AbstractJavaServletHtmlElement extends StringTextElement {

    AbstractLayoutComponent layoutComponent;
    String          tagName;

    String          text;

    HtmlTagAttributeList attributes = new HtmlTagAttributeList();

    public String encodeJavaQuotes(String string) {
        if ( string == null ) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for(int i=0; i < string.length(); i++) {
            sb.append(string.charAt(i) == '"' ? "&amp" : string.charAt(i));
        }

        return sb.toString();
    }

    public void requestWriteln(String string) {
        StringBuilder sb = new StringBuilder();
        if ( string != null ) {
            for ( int i=0; i < string.length(); i++) {
                sb.append(string.charAt(i) == '"' ? "\\\"" : string.charAt(i));
            }
        } else {
            sb.append("null");
        }
        pln("request.writeln(\"" + sb.toString() + "\");");
    }


    public AbstractLayoutComponent getLayoutComponent() {
        return layoutComponent;
    }

    public void setLayoutComponent(AbstractLayoutComponent layoutComponent) {
        this.layoutComponent = layoutComponent;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public HtmlTagAttributeList getAttributes() {
        return attributes;
    }

    protected void setAttribute(String name, String value) {
        attributes.set(name, value);
    }

    protected void setAttribute(String name) {
        attributes.set(name);
    }

    protected void unsetAttribute(String name) {
        attributes.unset(name);
    }

    protected void setClass(String className) {
        setAttribute("class", className);
    }

    protected void setRole(String role) {
        setAttribute("role", role);
    }

    protected void setType(String type) {
        setAttribute("type", type);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public <T extends AbstractElement> T addElement(Class<T> clazz, String childName) {
        T element = super.addElement(clazz, childName);
        ((StringTextElement) element).setLpad(getLpad() + "    ");
        return element;
    }

    protected String encodeAttributeValueQuotes(String str) {
        return str.replace("\"","&quot;");
    }

    @Override
    public void write() {


        StringBuilder sb = new StringBuilder();
        sb.append("request.writeln(\"<").append(getTagName());
        for ( HtmlTagAttribute a : getAttributes() ) {
            sb.append(" ")
                    .append(a.getName());
            if ( a.getValue() != null ) {
                sb.append("=\\\"")
                        .append(a.getValue())
                        .append("\\\"");
            }
        }

        if ( getElements().size() > 0 ) {
            sb.append(">");
            if ( getText() != null ) {
                sb.append(getText());
            }
            pln(sb.append("\");").toString());
            writeElements();
            requestWriteln("</" + getTagName() + ">");
        } else if ( getText() != null ) {
            pln(sb.append(">\");").toString());
            requestWriteln(getText());
            requestWriteln("</" + getTagName() + ">");
        } else {
            pln(sb.append("/>\");").toString());
        }

    }

}
