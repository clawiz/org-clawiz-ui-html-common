/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.html.element.common;


import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.common.view.form.FormGenerator;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.button.Button;
import org.clawiz.ui.common.metadata.data.layout.component.button.action.NoActionButtonAction;
import org.clawiz.ui.common.metadata.data.view.grid.button.action.CreateRowButtonAction;
import org.clawiz.ui.common.metadata.data.view.grid.button.action.DeleteRowButtonAction;
import org.clawiz.ui.common.metadata.data.view.grid.button.action.EditRowButtonAction;
import org.clawiz.ui.html.common.generator.html.element.AbstractHtmlBodyElement;

public class HtmlButtonElement extends AbstractHtmlBodyElement {

    Button               button;

    String               fontAwesomeName;
    boolean              useFontAwesome = true;

    public Button getButton() {
        return button;
    }

    @Override
    public void setLayoutComponent(AbstractLayoutComponent layoutComponent) {
        super.setLayoutComponent(layoutComponent);
        this.button = (Button) layoutComponent;

        setOnClickExpression(button.getOnClickExpression());
        if ( getOnClickExpression() == null ) {

            if ( button.getAction() == null || button.getAction() instanceof NoActionButtonAction ) {
                setOnClickExpression("onClick" + button.getJavaClassName() + "()");
            } else {
                String[] tokens = StringUtils.splitByLastTokenOccurrence(button.getAction().getClass().getName(), "\\.");
                int i = tokens[1].indexOf("ButtonAction");
                setOnClickExpression(i > 0 ? StringUtils.toLowerFirstChar(tokens[1].substring(0, i)) + "()" : null);

            }


        }

    }


    public boolean isUseFontAwesome() {
        return useFontAwesome;
    }

    public void setUseFontAwesome(boolean useFontAwesome) {
        this.useFontAwesome = useFontAwesome;
    }

    public String getFontAwesomeName() {
        if ( fontAwesomeName != null ) {
            return fontAwesomeName;
        }
        if ( button == null ) {
            return null;
        }


        if ( button.getAction() instanceof CreateRowButtonAction) {
            return "plus";
        } else if ( button.getAction() instanceof EditRowButtonAction) {
            return "pencil";
        } else if ( button.getAction() instanceof DeleteRowButtonAction) {
            return "trash";
        }

        return null;
    }

    public void setFontAwesomeName(String fontAwesomeName) {
        this.fontAwesomeName = fontAwesomeName;
    }

    public String getButtonCaption() {
        return getButton() != null && getButton().getText() != null ? getButton().getText() : null;
    }

    @Override
    public String getText() {
        String fa = "";
        if ( isUseFontAwesome() ) {
            String faName = getFontAwesomeName();
            if ( faName != null ) {
                fa = "<i class=\"fa fa-" + faName + "\"></i> ";
            }
        }
        return  fa  + ( getButtonCaption() != null ? getButtonCaption() : "" );
    }



    @SuppressWarnings("Duplicates")
    @Override
    public void process() {

        super.process();

        setTagName("button");

        if ( button != null && button.getName() != null) {
            setAttribute("name", button.getName());
        }

        setType("button");

        if (FormGenerator.class.isAssignableFrom(getGenerator().getClass())) {
            setAttribute("id", ((FormGenerator) getGenerator()).getNodeId(getButton()));
        }


    }

}
