/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.element.input;


import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.AbstractSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.StaticListSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.value.SelectValue;

public class JavaServletHtmlComboBoxInputlElement extends AbstractJavaServletHtmlInputElement {

    @Override
    public String getTagName() {
        return "select";
    }

    protected void addSelectOptions() {

        AbstractSelectValue vc = getFormField().getSelectValueContext();
        if ( vc == null ) {
            return;
        }

        if ( vc instanceof StaticListSelectValue) {
            StaticListSelectValue sc = (StaticListSelectValue) vc;
            for (SelectValue fc : sc.getValues()) {
                addElement(JavaServletHtmlSelectInputOptionElement.class).withValue(fc.getValue().toString());
            }
        } else {
            throwException("Wrong value change context class '?' of field '?'", new Object[]{vc.getClass().getName(), getFormField().getJavaClassName()});
        }

    }

    @Override
    public void process() {
        super.process();
        addSelectOptions();
    }
}
