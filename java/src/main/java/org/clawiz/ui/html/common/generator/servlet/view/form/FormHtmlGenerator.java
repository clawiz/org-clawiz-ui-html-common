/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.view.form;


import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeString;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.button.Button;
import org.clawiz.ui.common.metadata.data.layout.component.button.action.SubmitButtonAction;
import org.clawiz.ui.common.metadata.data.layout.component.field.Field;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.AbstractSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.StaticListSelectValue;
import org.clawiz.ui.common.generator.server.java.view.form.ServerFormGenerator;
import org.clawiz.ui.html.common.generator.servlet.view.form.controller.JavaScriptFormControllerComponent;
import org.clawiz.ui.html.common.generator.servlet.view.form.controller.JavaScriptFormControllerPrototypeComponent;
import org.clawiz.ui.html.common.generator.servlet.view.form.servlet.component.FormHtmlServletComponent;
import org.clawiz.ui.html.common.generator.servlet.view.form.servlet.component.FormHtmlServletPrototypeComponent;

public class FormHtmlGenerator extends ServerFormGenerator {

    FormHtmlServletComponent servletComponent;
    FormHtmlServletPrototypeComponent servletPrototypeComponent;

    JavaScriptFormControllerPrototypeComponent controllerPrototypeComponent;
    JavaScriptFormControllerComponent          controllerComponent;

    public FormHtmlServletComponent getHtmlServletComponent() {
        return servletComponent;
    }

    public FormHtmlServletPrototypeComponent getHtmlServletPrototypeComponent() {
        return servletPrototypeComponent;
    }


    public JavaScriptFormControllerPrototypeComponent getControllerPrototypeComponent() {
        return controllerPrototypeComponent;
    }

    public JavaScriptFormControllerComponent getControllerComponent() {
        return controllerComponent;
    }

    protected <T extends FormHtmlServletComponent> Class<T> getServletComponentClass() {
        return (Class<T>) FormHtmlServletComponent.class;
    }

    protected <T extends FormHtmlServletPrototypeComponent> Class<T> getServletPrototypeComponentClass() {
        return (Class<T>) FormHtmlServletPrototypeComponent.class;
    }

    protected <T extends JavaScriptFormControllerPrototypeComponent> Class<T> getControllerPrototypeComponentClass() {
        return (Class<T>) JavaScriptFormControllerPrototypeComponent.class;
    }

    protected <T extends JavaScriptFormControllerComponent> Class<T> getControllerComponentClass() {
        return (Class<T>) JavaScriptFormControllerComponent.class;
    }

    protected String getLayoutComponentPrefixedControllerPath(String _prefix, AbstractLayoutComponent component) {

        String path   = (_prefix != null ? _prefix.toLowerCase() : "clawiz") + "/ui/controller";
        String prefix = _prefix != null ? _prefix : "";
        String result = null;

        if ( component instanceof Field) {
            Field             field     = (Field) component;
            AbstractFormField formField = field.getFormField();

            if ( formField.getValueType() instanceof ValueTypeString) {
                result = "input/" +  prefix + "TextInputController";
            }

            if ( formField.getSelectValueContext() != null ) {
                AbstractSelectValue cvc = formField.getSelectValueContext();
                if ( cvc instanceof StaticListSelectValue) {
                    result = "input/" +  prefix + "StaticComboBoxInputController";
                } else {
                    result = "input/" +  prefix + "DynamicComboBoxInputController";
                }
            }
        }


        if ( component instanceof Button) {
            Button button = (Button) component;
            if ( button.getAction() instanceof SubmitButtonAction) {
                result = "button/form/" +  prefix + "SubmitFormButtonController";
            } else {
                result = "button/" +  prefix + "ButtonController";
            }
        }

        return result != null ? path + "/" + result : null;

    }

    public String getLayoutComponentControllerPath(AbstractLayoutComponent component) {
        return getLayoutComponentPrefixedControllerPath(null, component);
    }

    protected void addServlets() {
        servletComponent             = addComponent(getServletComponentClass());
        servletPrototypeComponent    = addComponent(getServletPrototypeComponentClass());

        controllerComponent          = addComponent(getControllerComponentClass());
        controllerPrototypeComponent = addComponent(getControllerPrototypeComponentClass());
    }

    @Override
    protected void process() {
        super.process();

        addServlets();
    }



}
