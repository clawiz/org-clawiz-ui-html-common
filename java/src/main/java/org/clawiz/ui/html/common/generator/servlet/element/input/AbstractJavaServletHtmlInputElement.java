/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.servlet.element.input;


import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDate;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDateTime;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeNumber;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.field.Field;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.html.common.generator.servlet.element.AbstractJavaServletHtmlElement;
import org.clawiz.ui.html.common.generator.servlet.element.JavaServletHtmlColumnElement;

public class AbstractJavaServletHtmlInputElement extends AbstractJavaServletHtmlElement {

    AbstractFormField formField;

    public AbstractFormField getFormField() {
        return formField;
    }

    public void setFormField(AbstractFormField formField) {
        this.formField = formField;
    }

    @Override
    public void setLayoutComponent(AbstractLayoutComponent layoutComponent) {
        super.setLayoutComponent(layoutComponent);
        Field field     = (Field) getLayoutComponent();
        setFormField(field.getFormField());
    }

    protected void setPlaceholder(String placeholder) {
        setAttribute("placeholder", placeholder);
    }

    protected boolean isParentContainerColumn() {
        return JavaServletHtmlColumnElement.class.isAssignableFrom(getParent().getClass());
    }


    protected boolean isLabelExistsInParentContainer() {
        if ( ! isParentContainerColumn() ) {
            return false;
        }

        for(AbstractElement element : ( getParent()).getElements() ){
            if ( element instanceof AbstractJavaServletHtmlInputElement) {
                if ( ((AbstractJavaServletHtmlInputElement) element).getFormField().getLabel() != null ) {
                    return true;
                }
            }
        }

        return false;
    }


    @Override
    public void process() {
        super.process();
        setTagName("input");

        setAttribute("name", formField.getJavaClassName());

        if ( formField.getHint() != null ) {
            setPlaceholder(formField.getHint());
        }

        if ( formField.isReadOnly() ) {
            setAttribute("readonly", null);
        }

        String getter = "request.getModel().get" + StringUtils.toUpperFirstChar(formField.getJavaClassName()) + "()";

        if ( formField.getValueType() instanceof ValueTypeDate) {
            getter = "dateToString(" + getter + ")";
        } else if ( formField.getValueType() instanceof ValueTypeDateTime ) {
            getter = "dateTimeToString(" + getter + ")";
        } else if ( formField.getValueType() instanceof ValueTypeNumber) {
            throwException("Need implement scale and precision");
        }

        getter = "notNull(" + getter + ", \"\")";

        setAttribute("value", "\" + " + getter + "+ \"");

    }


}
