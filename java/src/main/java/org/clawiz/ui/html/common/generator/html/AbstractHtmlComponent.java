/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.common.generator.html;

import org.clawiz.core.common.system.generator.file.component.AbstractTextFileComponent;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.FormLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.button.Button;
import org.clawiz.ui.common.metadata.data.layout.component.field.Field;
import org.clawiz.ui.common.metadata.data.layout.container.collection.IteratorCollection;
import org.clawiz.ui.common.metadata.data.layout.container.column.Column;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;
import org.clawiz.ui.common.metadata.data.layout.container.row.Row;
import org.clawiz.ui.common.metadata.data.layout.text.Text;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.html.common.generator.html.element.AbstractHtmlBodyElement;
import org.clawiz.ui.html.common.generator.html.element.collection.HtmlIteratorCollectionElement;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlButtonElement;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlDivElement;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlTextElement;
import org.clawiz.ui.html.common.generator.html.view.form.element.HtmlFormContainerElement;
import org.clawiz.ui.html.common.generator.html.view.form.element.HtmlFormFieldElement;

import java.util.HashMap;

public class AbstractHtmlComponent extends AbstractTextFileComponent {

    HashMap<Class<AbstractLayoutComponent>, Class<AbstractHtmlBodyElement>> layoutComponentsMap = new HashMap<>();

    public AbstractHtmlBodyElement addLayoutComponent(AbstractLayoutComponent layoutComponent) {
        return addLayoutComponent(null, layoutComponent, true);
    }

    public AbstractHtmlBodyElement addLayoutComponent(AbstractHtmlBodyElement parentElement, AbstractLayoutComponent layoutComponent, boolean processChilds) {

        Class<AbstractHtmlBodyElement> clazz = layoutComponentsMap.get(layoutComponent.getClass());
        if ( clazz == null ) {
            for (Class key : layoutComponentsMap.keySet()) {
                if ( key.isAssignableFrom(layoutComponent.getClass())) {
                    if ( clazz != null ) {
                        throwException("Layout component map not contain key and have more than one superclass for ?", layoutComponent.getClass());
                    }
                    clazz = layoutComponentsMap.get(key);
                }
            }
            layoutComponentsMap.put((Class<AbstractLayoutComponent>) layoutComponent.getClass(), clazz);
        }

        if ( clazz == null ) {
            throwException("Class map for layout component ? not defined", layoutComponent.getClass().getName());
        }

        AbstractHtmlBodyElement element = parentElement == null ? addElement(clazz) : parentElement.addElement(clazz);
        element.setLayoutComponent(layoutComponent);

        if ( processChilds && layoutComponent instanceof AbstractContainer) {
            for(AbstractLayoutComponent child : ((AbstractContainer) layoutComponent).getComponents()) {
                addLayoutComponent(element, child, true);
            }
        }

        return element;
    }

    protected <L extends AbstractLayoutComponent, E extends AbstractHtmlBodyElement> void  addLayoutComponentMap(Class<L> componentClass, Class<E> htmlElementClass ) {
        layoutComponentsMap.put((Class<AbstractLayoutComponent>) componentClass, (Class<AbstractHtmlBodyElement>) htmlElementClass);
    }

    protected void prepareLayoutComponentsMap() {

        addLayoutComponentMap(Row.class, HtmlDivElement.class);
        addLayoutComponentMap(Column.class, HtmlDivElement.class);
        addLayoutComponentMap(IteratorCollection.class, HtmlIteratorCollectionElement.class);
        addLayoutComponentMap(FormLayoutComponent.class, HtmlFormContainerElement.class);
        addLayoutComponentMap(Field.class, HtmlFormFieldElement.class);
        addLayoutComponentMap(Button.class, HtmlButtonElement.class);
        addLayoutComponentMap(Text.class, HtmlTextElement.class);

    }

    @Override
    public void setName(String name) {
        super.setName(name);
        setFileName(name + ".html");
    }

    @Override
    public void prepare() {
        super.prepare();
        prepareLayoutComponentsMap();
    }
}
