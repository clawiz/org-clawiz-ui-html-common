/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Controller} from "../Controller"

export class ViewController extends Controller {

    processLoadQueue(parameters) {

        let queue    = parameters.queue;
        let success  = parameters.success;

        let me = this;

        let loadQueueSize = queue.length;

        function decLoadQueue() {
            loadQueueSize--;
            if ( loadQueueSize == 0 ) {
                me.logDebug("All controllers loaded");
                if ( success != null ) {
                    success();
                }
            }
        }


        for(let i=0; i < queue.length; i++) {

            let job = queue[i];


            let path       = job.path;
            let element    = job.element;
            let parameters = job.parameters;

            if ( path != null ) {

                this.requirejs([path]
                    ,function(arg) {

                        let tokens         = path.split('/');
                        let name           = tokens[tokens.length-1];
                        let controller     = (new arg[name]);
                        let elementName    = element.getAttribute("name");

                        controller.element = element;
                        controller.parent  = me;

                        controller.applyParameters(parameters);
                        controller.init();

                        element.clawizController = controller;

                        me[elementName] = controller;
                        me.logDebug("Success load " + path + " for " + elementName);
                        decLoadQueue();
                    }
                )

            } else {
                throw 'Wrong load queue job type' + job.toString();
            }

        }

    }

}