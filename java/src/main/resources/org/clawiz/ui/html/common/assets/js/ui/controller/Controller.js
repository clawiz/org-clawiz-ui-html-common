/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

define(['exports', '../UIBase'], function (exports, _UIBase2) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.Controller = undefined;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var Controller = exports.Controller = function (_UIBase) {
        _inherits(Controller, _UIBase);

        function Controller() {
            _classCallCheck(this, Controller);

            return _possibleConstructorReturn(this, (Controller.__proto__ || Object.getPrototypeOf(Controller)).apply(this, arguments));
        }

        _createClass(Controller, [{
            key: 'applyParameters',
            value: function applyParameters(parameters) {

                if (parameters == null) {
                    return;
                }

                for (var key in parameters) {
                    this[key] = parameters[key];
                }
            }
        }, {
            key: 'callApiLock',
            value: function callApiLock() {
                this.logDebug("API lock for " + this);
            }
        }, {
            key: 'callApiUnlock',
            value: function callApiUnlock() {
                this.logDebug("API unlock for " + this);
            }
        }, {
            key: 'callApiDone',
            value: function callApiDone(data, response) {
                this.logDebug("Api done for " + this);
            }
        }, {
            key: 'callApiFail',
            value: function callApiFail(response) {
                this.logDebug("Api fail for " + this);
            }
        }, {
            key: '_getParametersCallApiMethod',
            value: function _getParametersCallApiMethod(name, parameters) {
                var key = "callApi" + name;
                if (parameters[key] != null) {
                    return parameters.controller != null ? parameters[key].bind(parameters.controller) : parameters[key];
                }
                var controller = parameters.controller;
                if (controller != null) {
                    if (controller[key] != null) {
                        return controller[key].bind(controller);
                    }
                }
                if (this[key] != null) {
                    return this[key].bind(this);
                }
                return this._emptyFunction;
            }
        }, {
            key: 'processApiResponseActions',
            value: function processApiResponseActions(response) {
                if (response == null || response.actions == null) {
                    return;
                }

                var me = this;

                response.actions.forEach(function (value) {

                    if (value.type == 'redirect') {
                        var url = value.url;
                        window.location.href = url;
                    } else {
                        this.throwError('Wrong api response action type "' + value.type + "'");
                    }
                });
            }

            /*
                getCookie(name) {
                    let value = "; " + document.cookie;
                    let parts = value.split("; " + name + "=");
                    if (parts.length == 2) return parts.pop().split(";").shift();
                }
            
                getSessionIdParameterName() {
                    return 'cwsessionid';
                }
            
            */

        }, {
            key: 'callApi',
            value: function callApi(parameters) {

                var data = parameters.data != null ? parameters.data : {};
                if (parameters.action != null) {
                    data.action = parameters.action;
                }

                // data[this.getSessionIdParameterName()]= this.getCookie(this.getSessionIdParameterName());

                var me = this;

                var callApiLock = me._getParametersCallApiMethod('Lock', parameters);
                var callApiUnlock = me._getParametersCallApiMethod('Unlock', parameters);

                var callApiDone = me._getParametersCallApiMethod('Done', parameters);
                var callApiFail = me._getParametersCallApiMethod('Fail', parameters);

                var callParameters = {
                    type: "POST",
                    url: me.apiUrl,
                    data: data,
                    dataType: "json"
                };

                callApiLock();

                this.jQuery.ajax(callParameters).done(function (response) {

                    me.processApiResponseActions(response);

                    if (response.status.type == 'OK') {

                        callApiDone(response.data, response);
                    } else {

                        callApiFail(response.data, response);
                    }

                    callApiUnlock();
                }).fail(function (response) {

                    callApiFail(response);
                    callApiUnlock();
                });
            }
        }, {
            key: 'init',
            value: function init() {}
        }, {
            key: 'element',
            set: function set(element) {
                this._element = element;
            },
            get: function get() {
                return this._element;
            }
        }, {
            key: 'jQueryElement',
            get: function get() {
                return this.jQuery('#' + this.element.id);
            }
        }, {
            key: 'name',
            get: function get() {
                return element != null ? element.name : null;
            }
        }]);

        return Controller;
    }(_UIBase2.UIBase);
});

//# sourceMappingURL=Controller.js.map