/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

define(["exports", "../Controller"], function (exports, _Controller2) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.ViewController = undefined;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var ViewController = exports.ViewController = function (_Controller) {
        _inherits(ViewController, _Controller);

        function ViewController() {
            _classCallCheck(this, ViewController);

            return _possibleConstructorReturn(this, (ViewController.__proto__ || Object.getPrototypeOf(ViewController)).apply(this, arguments));
        }

        _createClass(ViewController, [{
            key: "processLoadQueue",
            value: function processLoadQueue(parameters) {
                var _this2 = this;

                var queue = parameters.queue;
                var success = parameters.success;

                var me = this;

                var loadQueueSize = queue.length;

                function decLoadQueue() {
                    loadQueueSize--;
                    if (loadQueueSize == 0) {
                        me.logDebug("All controllers loaded");
                        if (success != null) {
                            success();
                        }
                    }
                }

                var _loop = function _loop(i) {

                    var job = queue[i];

                    var path = job.path;
                    var element = job.element;
                    var parameters = job.parameters;

                    if (path != null) {

                        _this2.requirejs([path], function (arg) {

                            var tokens = path.split('/');
                            var name = tokens[tokens.length - 1];
                            var controller = new arg[name]();
                            var elementName = element.getAttribute("name");

                            controller.element = element;
                            controller.parent = me;

                            controller.applyParameters(parameters);
                            controller.init();

                            element.clawizController = controller;

                            me[elementName] = controller;
                            me.logDebug("Success load " + path + " for " + elementName);
                            decLoadQueue();
                        });
                    } else {
                        throw 'Wrong load queue job type' + job.toString();
                    }
                };

                for (var i = 0; i < queue.length; i++) {
                    _loop(i);
                }
            }
        }]);

        return ViewController;
    }(_Controller2.Controller);
});

//# sourceMappingURL=ViewController.js.map