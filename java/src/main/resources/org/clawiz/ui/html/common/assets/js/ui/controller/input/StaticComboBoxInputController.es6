/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {AbstractComboBoxInputController} from "./AbstractComboBoxInputController"


export class StaticComboBoxInputController extends AbstractComboBoxInputController {

    valueToText(value) {
        let vo = this.valueToValueObject(value);
        return vo != null ? vo.text : null;
    }

    textToValue(text) {
        let vo = this.textToValueObject(text);
        return vo != null ? vo.value : null;
    }

    valueToValueObject(value) {
        if ( value != null ) {
            return this.valuesMap[value];
        }
        return null;
    }

    textToValueObject(text) {
        if ( text != null ) {
            return this.textsMap[text];
        }
        return null;
    }

    init() {
        super.init();

        let me = this;

        let valuesMap = {};
        let textsMap  = {};

        if ( me.values != null ) {
            for(let i=0; i < me.values.length; i++ ) {
                let value = me.values[i];
                if ( value == null ) {
                    continue;
                }
                if ( value.value != null ) {
                    valuesMap[value.value] = value;
                }
                if ( value.text != null ) {
                    textsMap[value.text] = value;
                }
            }
        }

        this.valuesMap = valuesMap;
        this.textsMap  = textsMap;
    }

}