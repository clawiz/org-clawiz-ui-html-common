/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

define(["exports", "../ViewController"], function (exports, _ViewController2) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.FormController = undefined;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    var _get = function get(object, property, receiver) {
        if (object === null) object = Function.prototype;
        var desc = Object.getOwnPropertyDescriptor(object, property);

        if (desc === undefined) {
            var parent = Object.getPrototypeOf(object);

            if (parent === null) {
                return undefined;
            } else {
                return get(parent, property, receiver);
            }
        } else if ("value" in desc) {
            return desc.value;
        } else {
            var getter = desc.get;

            if (getter === undefined) {
                return undefined;
            }

            return getter.call(receiver);
        }
    };

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var FormController = exports.FormController = function (_ViewController) {
        _inherits(FormController, _ViewController);

        function FormController() {
            _classCallCheck(this, FormController);

            return _possibleConstructorReturn(this, (FormController.__proto__ || Object.getPrototypeOf(FormController)).apply(this, arguments));
        }

        _createClass(FormController, [{
            key: "getFormData",
            value: function getFormData() {}
        }, {
            key: "_emptyFunction",
            value: function _emptyFunction() {}
        }, {
            key: "processLoadQueue",
            value: function processLoadQueue(parameters) {
                var me = this;
                parameters.queue.forEach(function (value) {
                    value.parameters.formController = me;
                });
                _get(FormController.prototype.__proto__ || Object.getPrototypeOf(FormController.prototype), "processLoadQueue", this).call(this, parameters);
            }
        }, {
            key: "onLoadControllers",
            value: function onLoadControllers() {}
        }, {
            key: "callApi",
            value: function callApi(parameters) {
                var data = parameters.data;

                if (data == null) {
                    data = this.getFormData();
                }
                if (data == null) {
                    data = {};
                }

                parameters.data = data;

                _get(FormController.prototype.__proto__ || Object.getPrototypeOf(FormController.prototype), "callApi", this).call(this, parameters);
            }
        }, {
            key: "jQueryForm",
            set: function set(jQueryForm) {
                this._jQueryForm = jQueryForm;
            },
            get: function get() {
                return this._jQueryForm;
            }
        }, {
            key: "form",
            set: function set(form) {
                this._form = form;
            },
            get: function get() {
                return this._form;
            }
        }, {
            key: "apiUrl",
            set: function set(formApiUrl) {
                this._formApiUrl = formApiUrl;
            },
            get: function get() {
                return this._formApiUrl;
            }
        }, {
            key: "servletUrl",
            set: function set(formServletUrl) {
                this._formServletUrl = formServletUrl;
            },
            get: function get() {
                return this._formServletUrl;
            }
        }]);

        return FormController;
    }(_ViewController2.ViewController);
});

//# sourceMappingURL=FormController.js.map