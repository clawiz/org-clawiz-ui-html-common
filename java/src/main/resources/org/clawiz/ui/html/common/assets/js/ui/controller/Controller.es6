/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {UIBase} from '../UIBase'

export class Controller extends UIBase {

    applyParameters(parameters) {

        if ( parameters == null ) {
            return;
        }

        for(let key in parameters ) {
            this[key] = parameters[key]
        }

    }

    set element(element) {
        this._element = element;
    }

    get element() {
        return this._element;
    }

    get jQueryElement() {
        return this.jQuery('#' + this.element.id);
    }

    get name() {
        return element != null ? element.name : null;
    }

    callApiLock() {
        this.logDebug("API lock for " + this);
    }

    callApiUnlock() {
        this.logDebug("API unlock for " + this);
    }

    callApiDone(data, response) {
        this.logDebug("Api done for " + this);
    }

    callApiFail(response) {
        this.logDebug("Api fail for " + this);
    }

    _getParametersCallApiMethod(name, parameters) {
        let key = "callApi" + name;
        if ( parameters[key] != null ) {
            return parameters.controller != null ? parameters[key].bind(parameters.controller) : parameters[key];
        }
        let controller = parameters.controller;
        if ( controller != null ) {
            if ( controller[key] != null ) {
                return controller[key].bind(controller);
            }
        }
        if ( this[key] != null ) {
            return this[key].bind(this);
        }
        return this._emptyFunction;
    }


    processApiResponseActions(response) {
        if ( response == null || response.actions == null ) {
            return;
        }

        let me = this;

        response.actions.forEach(function(value) {

            if ( value.type == 'redirect' ) {
                let url = value.url;
                window.location.href = url;
            } else {
                this.throwError('Wrong api response action type "' + value.type + "'");
            }

        });

    }

/*
    getCookie(name) {
        let value = "; " + document.cookie;
        let parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }

    getSessionIdParameterName() {
        return 'cwsessionid';
    }

*/
    callApi(parameters) {

        let data          = parameters.data != null ? parameters.data : {};
        if ( parameters.action != null ) {
            data.action = parameters.action;
        }

        // data[this.getSessionIdParameterName()]= this.getCookie(this.getSessionIdParameterName());

        let me = this;

        let callApiLock   = me._getParametersCallApiMethod('Lock', parameters);
        let callApiUnlock = me._getParametersCallApiMethod('Unlock', parameters);

        let callApiDone   = me._getParametersCallApiMethod('Done', parameters);
        let callApiFail   = me._getParametersCallApiMethod('Fail', parameters);

        let callParameters = {
            type     : "POST",
            url      : me.apiUrl,
            data     : data,
            dataType : "json"
        };

        callApiLock();

        this.jQuery.ajax(callParameters)

            .done(function(response) {

                me.processApiResponseActions(response);

                if ( response.status.type == 'OK') {

                    callApiDone(response.data, response)

                } else {

                    callApiFail(response.data, response)

                }

                callApiUnlock();

            })

            .fail(function(response) {

                callApiFail(response)
                callApiUnlock();

            });

    }

    init() {
    }

}