/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

define(["exports", "../../BaseLoader", "./input/InputControllerLoader", "./view/ViewControllerLoader"], function (exports, _BaseLoader2, _InputControllerLoader, _ViewControllerLoader) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.ControllerLoader = undefined;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    var _get = function get(object, property, receiver) {
        if (object === null) object = Function.prototype;
        var desc = Object.getOwnPropertyDescriptor(object, property);

        if (desc === undefined) {
            var parent = Object.getPrototypeOf(object);

            if (parent === null) {
                return undefined;
            } else {
                return get(parent, property, receiver);
            }
        } else if ("value" in desc) {
            return desc.value;
        } else {
            var getter = desc.get;

            if (getter === undefined) {
                return undefined;
            }

            return getter.call(receiver);
        }
    };

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var ControllerLoader = exports.ControllerLoader = function (_BaseLoader) {
        _inherits(ControllerLoader, _BaseLoader);

        function ControllerLoader() {
            _classCallCheck(this, ControllerLoader);

            return _possibleConstructorReturn(this, (ControllerLoader.__proto__ || Object.getPrototypeOf(ControllerLoader)).apply(this, arguments));
        }

        _createClass(ControllerLoader, [{
            key: "init",
            value: function init() {
                _get(ControllerLoader.prototype.__proto__ || Object.getPrototypeOf(ControllerLoader.prototype), "init", this).call(this);

                this._inputControllerLoader = new _InputControllerLoader.InputControllerLoader();
                this._viewControllerLoader = new _ViewControllerLoader.ViewControllerLoader();
            }
        }, {
            key: "View",
            get: function get() {
                return this._viewControllerLoader;
            }
        }, {
            key: "Input",
            get: function get() {
                return this._inputControllerLoader;
            }
        }]);

        return ControllerLoader;
    }(_BaseLoader2.BaseLoader);
});

//# sourceMappingURL=ControllerLoader.js.map