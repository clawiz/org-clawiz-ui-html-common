/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Controller} from "../Controller"

export class InputController extends Controller {


    set element(element) {
        super.element = element;
        this.input    = element;
    }

    get element() {
        return super.element;
    }

    get input() {
        return this._input;
    }

    set input(value) {
        this._input = value;
        value.clawizController = this;
    }

    valueToText(value) {
        return value;
    }

    textToValue(text) {
        return text;
    }

    set inputText(text) {
        this.input.value = text;
    }

    get inputText() {
        return this.input.value;
    }

    get jQueryInput() {
        if ( this._jQueryInput == null ) {
            this._jQueryInput = this.jQuery('#' + this.input.id);
        }
        return this._jQueryInput;
    }

    set value(value) {

        let inputText  = null;
        let inputValue = null;

        if ( value instanceof Object) {
            inputText  = value.text;
            inputValue = value.value;
        } else {
            inputValue = value;
            inputText  = this.valueToText(value);
        }

        this.inputText    = inputText;
        this.inputValue   = inputValue;
        this.isLocalValue = true;

        this.logDebug(this.input.name + " = '" + inputValue + "':'" + inputText + "'");
    }

    get value() {
        if ( this.isLocalValue ) {
            return this.inputValue;
        } else {
            return  this.input.value;
        }
    }



}