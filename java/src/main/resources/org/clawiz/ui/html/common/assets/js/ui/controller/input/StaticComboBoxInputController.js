/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

define(["exports", "./AbstractComboBoxInputController"], function (exports, _AbstractComboBoxInputController) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.StaticComboBoxInputController = undefined;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    var _get = function get(object, property, receiver) {
        if (object === null) object = Function.prototype;
        var desc = Object.getOwnPropertyDescriptor(object, property);

        if (desc === undefined) {
            var parent = Object.getPrototypeOf(object);

            if (parent === null) {
                return undefined;
            } else {
                return get(parent, property, receiver);
            }
        } else if ("value" in desc) {
            return desc.value;
        } else {
            var getter = desc.get;

            if (getter === undefined) {
                return undefined;
            }

            return getter.call(receiver);
        }
    };

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var StaticComboBoxInputController = exports.StaticComboBoxInputController = function (_AbstractComboBoxInpu) {
        _inherits(StaticComboBoxInputController, _AbstractComboBoxInpu);

        function StaticComboBoxInputController() {
            _classCallCheck(this, StaticComboBoxInputController);

            return _possibleConstructorReturn(this, (StaticComboBoxInputController.__proto__ || Object.getPrototypeOf(StaticComboBoxInputController)).apply(this, arguments));
        }

        _createClass(StaticComboBoxInputController, [{
            key: "valueToText",
            value: function valueToText(value) {
                var vo = this.valueToValueObject(value);
                return vo != null ? vo.text : null;
            }
        }, {
            key: "textToValue",
            value: function textToValue(text) {
                var vo = this.textToValueObject(text);
                return vo != null ? vo.value : null;
            }
        }, {
            key: "valueToValueObject",
            value: function valueToValueObject(value) {
                if (value != null) {
                    return this.valuesMap[value];
                }
                return null;
            }
        }, {
            key: "textToValueObject",
            value: function textToValueObject(text) {
                if (text != null) {
                    return this.textsMap[text];
                }
                return null;
            }
        }, {
            key: "init",
            value: function init() {
                _get(StaticComboBoxInputController.prototype.__proto__ || Object.getPrototypeOf(StaticComboBoxInputController.prototype), "init", this).call(this);

                var me = this;

                var valuesMap = {};
                var textsMap = {};

                if (me.values != null) {
                    for (var i = 0; i < me.values.length; i++) {
                        var value = me.values[i];
                        if (value == null) {
                            continue;
                        }
                        if (value.value != null) {
                            valuesMap[value.value] = value;
                        }
                        if (value.text != null) {
                            textsMap[value.text] = value;
                        }
                    }
                }

                this.valuesMap = valuesMap;
                this.textsMap = textsMap;
            }
        }]);

        return StaticComboBoxInputController;
    }(_AbstractComboBoxInputController.AbstractComboBoxInputController);
});

//# sourceMappingURL=StaticComboBoxInputController.js.map