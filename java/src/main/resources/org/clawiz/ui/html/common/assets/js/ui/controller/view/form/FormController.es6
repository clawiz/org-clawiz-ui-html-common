/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ViewController} from "../ViewController"

export class FormController extends ViewController {

    set jQueryForm(jQueryForm) {
        this._jQueryForm = jQueryForm;
    }

    get jQueryForm() {
        return this._jQueryForm;
    }

    set form(form) {
        this._form = form;
    }

    get form() {
        return this._form;
    }

    set apiUrl(formApiUrl) {
        this._formApiUrl = formApiUrl;
    }

    get apiUrl() {
        return this._formApiUrl;
    }

    set servletUrl(formServletUrl) {
        this._formServletUrl = formServletUrl;
    }

    get servletUrl() {
        return this._formServletUrl;
    }

    getFormData() {

    }

    _emptyFunction() {

    }

    processLoadQueue(parameters) {
        let me = this;
        parameters.queue.forEach(function(value) {
            value.parameters.formController = me;
        });
        super.processLoadQueue(parameters);
    }

    onLoadControllers() {

    }

    callApi(parameters) {
        let data   = parameters.data;

        if ( data == null ) {
            data = this.getFormData();
        }
        if ( data == null ) {
            data = {};
        }

        parameters.data = data;

        super.callApi(parameters);

    }

}